import worstCMS.modules.tools as tools

def compute(data):
    # statics for posts
    for site in data["posts"]:
        html_content = tools.wrap_html_content(tools.json2html_static(site["text"], site["title"]), data["html"])
        filename = site["title"] + ".html"
        with open(data["out_folder"] + filename, "w") as result_file:
            import html5lib
            html5parser = html5lib.HTMLParser(strict=True)
            if html5parser.parse(html_content):
                print("html5 yes")
                result_file.write(html_content)    
        html_content = ""