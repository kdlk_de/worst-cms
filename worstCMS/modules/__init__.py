import os
import sys
PROJECT_PATH = os.getcwd()
SOURCE_PATH = os.path.join(
    # SOURCE_PATH second parameter usually is set to: worstCMS
    # get for full path tools module worstCMS.module.tools
    # import modules.tools works, but it shows an error that the module is not found
    PROJECT_PATH,".."
)
sys.path.append(SOURCE_PATH)

