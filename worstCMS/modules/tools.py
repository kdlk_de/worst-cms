def wrap_html_content(part_to_wrap, html_data):
    full_html = ""
    full_html += html_data["header"]
    full_html += html_data["navigation"]
    full_html += html_data["between_header_main"]
    full_html += part_to_wrap
    full_html += html_data["aside"]
    full_html += html_data["footer_start"]
    full_html += html_data["footer"]
    full_html += html_data["footer_end"]
    return full_html


def check_json(json_dict):
    if ("title" in json_dict.keys() 
        and "text" in json_dict.keys() 
        and "tags" in json_dict.keys()
        and "timestamp" in json_dict.keys()):
        return True
    else:
        return False

def json2html_index(json_dict):
    if not check_json(json_dict):
        return ""
    html = ""
    html += "<article>"
    html += "<h3>"
    html += "<a href=\"./index.html\">"
    html += json_dict["title"]
    html += "</a>"
    html += "</h3>"
    for part in json_dict["text"]:
        html += format_part(part)
    html += "<address>"
    html += "<time datetime=\""
    html += json_dict["timestamp"]
    html += "\">"
    #html += "tag monat jahr"
    html += "</time>"
    html += "<br />"
    html += "Tags: "
    for tag in json_dict["tags"]:
        html += "<a href=\"#\" style=\"color:blue;\">#"
        html += tag
        html += " </a>"
    if "source_link" in json_dict.keys():
        html += "<br />"
        html += "Source: <a href=\""
        html += json_dict["source_link"] +"\""
        html += " style=\"color:blue;\">"
        if "source_text" in json_dict.keys():
            html += json_dict["source_text"]
        else:
            html += json_dict["source_link"]
        html += "</a>"
    html += "</address>"
    html += "</article>"
    return html

def json2html_static(json_list, title):
    html = ""
    html += "<article>"
    html += "<h3 class='center_text'>"
    html +=  title
    html += "</h3>"
    for part in json_list:
        html += format_part(part)
    html += "</article>"
    return html

def json2html_aside(json_list):
    html = ""
    html += "</div>"
    html += "</main>"
    html += "<aside>"
    for part in json_list:
        html += format_part(part)
    html += "</aside>"
    return html

def format_part(part):
    if part.startswith("-text-:"):
        return "<p>" + part[len("-text-:"):] + "</p>"
    elif part.startswith("-title-:"):
        return "<h3>" + part[len("-title-:"):] + "</h3>"
    elif part.startswith("-full_title-:"):
        return "<h2>" + part[len("-full_title-:"):] + "</h2>"
    elif part.startswith("-image-:"):
        split_part = part[len("-image-:"):].split("|")
        return "<div class='center_text'><div class='image_wrap'><img src='" + split_part[0] + "' alt='" + split_part[1] + "'><br/><span>" + split_part[1] + "</span></div></div>"
    elif part.startswith("-link-:"):
        split_part = part[len("-link-:"):].split("|")
        return "<a href='" + split_part[0] + "' alt='" + split_part[1] + "'>" + split_part[1] + "</a><br />"
    elif part.startswith("-quote-:"):
        return "<blockquote>" + part[len("-quote-:"):] + "</blockquote>"
    elif part.startswith("-code-:"):
        return "<blockquote> code:" + part[len("-code-:"):] + "</blockquote>"
    elif part.startswith("-tweet-:"):
        split_part = part[len("-image-:"):].split("|")
        return "<iframe src='iframes/feed." + split_part[0] + ".html' frameborder='0' scrolling='no' style='width: 100%;height:" + split_part[1] + "px;'></iframe>"
    return part