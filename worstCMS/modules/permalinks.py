import worstCMS.modules.tools as tools

def replace_relative_paths(html):
    return html.replace("./", "../")

def compute(data):
    for site in data["permalinks"]:
        html_content = tools.wrap_html_content(tools.json2html_static(site["text"], site["title"]), data["html"])
        filename = site["title"] + ".html"
        with open(data["out_folder"] + "/permalinks/" + filename, "w") as result_file:
            import html5lib
            html5parser = html5lib.HTMLParser(strict=True)
            if html5parser.parse(html_content):
                print("html5 yes")
                html_content = replace_relative_paths(html_content)
                result_file.write(html_content)    
        html_content = ""