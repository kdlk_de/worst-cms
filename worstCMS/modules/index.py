import worstCMS.modules.tools as tools

def compute(data):
    # # data
    # index.html
    # shorten for preview
    index_posts = ""
    for post in data["posts"]:
        # print(post)
        index_posts += tools.json2html_index(post)
    full_index_html = tools.wrap_html_content(index_posts, data["html"])
    with open(data["out_folder"] + "index.html", "w") as result_file:
        # [TODO]fku: testcase html parsing
        import html5lib
        html5parser = html5lib.HTMLParser(strict=True)
        if html5parser.parse(full_index_html):
            print("html5 yes")
            result_file.write(full_index_html)