import worstCMS.modules.tools as tools

def wrap_navigation_link(site):
    return f"<li><a href='./permalinks/{site}.html'>{site}</a></li>"

def compute(data):
    navigation_html = "<nav><ul>"
    for site in data["permalinks"]:
        navigation_html += wrap_navigation_link(site["title"])

    navigation_html += "</ul></nav>"
    return navigation_html