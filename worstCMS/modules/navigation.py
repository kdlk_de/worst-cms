import worstCMS.modules.tools as tools

def wrap_navigation_link(site):
    return f"<a href='./{site}.html'>{site}</a>"

def compute(data):
    navigation_html = "<div>"
    elements = [wrap_navigation_link("index")]
    for site in data["statics"]:
        elements.append(wrap_navigation_link(site["title"]))
    navigation_html += " | ".join(elements)
    navigation_html += "</div>"
    return navigation_html