import json
import time

from os import path,makedirs

import importlib

def create_output_folder(foldername):
    makedirs(foldername)

def file_availability(folder):
    file_missing = False
    if not path.exists(folder + "posts.json"):
        file_missing = True
    if not path.exists(folder + "statics.json"):
        file_missing = True
    if file_missing:
        exit("Please check the ouput above for the missing files.")

def fetch_data(folder, json_part):
    json_file = open(folder + json_part + ".json","r")
    json_elements = json.load(json_file)
    json_file.close()
    return json_elements

def get_text_from_file(file_part):
    file_extension = ".html"
    data_path = data["folder"]
    filename = data_path + "html/" + file_part + file_extension
    if not path.exists(filename):
        exit("there is no such file: " + filename)
    with open(filename, "r") as html_file:
        return html_file.read()
    return ""

def get_header_data():
    return get_text_from_file("header")

def get_footer_start_data():
    return get_text_from_file("footer_start")

def get_footer_end_data():
    return get_text_from_file("footer_end")

def get_aside_data():
    return get_text_from_file("aside")

def get_between_header_main_data():
    return get_text_from_file("between_header_main")

def copy_folders(source, destination):
    import subprocess
    subprocess.call(['sh', './copy_script.sh', source, destination])

if __name__ == "__main__":
    # example CLI: python cms.py --output=res --input=data --parts=index,statics,posts
    # command line arguments
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--output", help="Set the name for the output folder. Default is \"results\".")
    parser.add_argument("--input", help="Set the name for the input folder. Default is \"data\".")
    parser.add_argument("--parts", help="Comma-separated List with the parts to be created. Default creates every part.")
    parser.add_argument("--verbose", help="Set the name for the input folder")

    args = parser.parse_args()


    # prepare data
    data = {}
    data["folder"] = "../data/" if args.input == None else "../" + str(args.input) + "/"
    data["out_folder"] = "../results/"  if args.output == None else "../" + str(args.output) + "/"

    data["parts"] = ["index", "statics", "posts", "permalinks"] if args.parts == None else args.parts.split(",")

    data["posts"] = {}
    data["aside"] = {}
    data["statics"] = {}
    data["between_header_main"] = {}
    data["footer"] = {}
    data["html"] = {"header": "", "footer": "", "aside": "", "full": ""}
    print("file", data["folder"])
    file_availability(data["folder"])
    data["posts"] = fetch_data(data["folder"], "posts")
    data["statics"] = fetch_data(data["folder"], "statics")
    data["permalinks"] = fetch_data(data["folder"], "permalinks")

    data["html"]["header"] = get_header_data()
    data["html"]["footer_start"] = get_footer_start_data()
    data["html"]["footer_end"] = get_footer_end_data()
    data["html"]["aside"] = get_aside_data()
    data["html"]["between_header_main"] = get_between_header_main_data()

    module = importlib.import_module("modules." + "navigation")  
    data["html"]["navigation"] = module.compute(data)
    module = importlib.import_module("modules." + "permalinks_navigation")  
    data["html"]["footer"] = module.compute(data)
    
    if not path.exists(data["out_folder"]):
        create_output_folder(data["out_folder"])
        if not path.exists(data["out_folder"] + "/permalinks"):
            create_output_folder(data["out_folder"] + "/permalinks")


    for part in data["parts"]:
        module = importlib.import_module("modules." + part)  
        module.compute(data)

    copy_folders(data["folder"], data["out_folder"])