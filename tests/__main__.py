import unittest
from unittest.mock import patch, mock_open
import json
from worstCMS import cms
from worstCMS.modules import tools

class TestStringMethods(unittest.TestCase):

    @patch("builtins.open", new_callable=mock_open,
       read_data=json.dumps({'name' : 'John','shares' : 100,
                        'price' : 1230.23}))
    def test_fetch_data(self, mocked_file):
        expected_output = {
                  'name' : 'John',
                  'shares' : 100,
                  'price' : 1230.23
                  }
        filename = 'example.json'
        actual_output = cms.fetch_data(filename, 'example/path')
        print(mocked_file)
        self.assertEqual(expected_output, actual_output)

    def test_format_part_on_empty(self):
        expected_output = ""
        self.assertEqual(tools.format_part(""), expected_output)

    def test_format_part_text(self):
        expected_output = "<p>Hello, World!</p>"
        self.assertEqual(tools.format_part("-text-:Hello, World!"), expected_output)

    def test_format_part_title(self):
        expected_output = "<h3>Hello, World!</h3>"
        self.assertEqual(tools.format_part("-title-:Hello, World!"), expected_output)

    def test_format_part_full_title(self):
        expected_output = "<h2>Hello, World!</h2>"
        self.assertEqual(tools.format_part("-full_title-:Hello, World!"), expected_output)

    def test_format_part_quote(self):
        expected_output = "<blockquote>Hello, World!</blockquote>"
        self.assertEqual(tools.format_part("-quote-:Hello, World!"), expected_output)

    def test_format_part_link(self):
        expected_output = "<a href='https://forum.manjaro.org/t/manjaro-arm-beta11-with-phosh-pinephone/69068' alt='ManjaroARM phosh beta 11'>ManjaroARM phosh beta 11</a><br />"
        self.assertEqual(tools.format_part("-link-:https://forum.manjaro.org/t/manjaro-arm-beta11-with-phosh-pinephone/69068|ManjaroARM phosh beta 11"), expected_output)

    def test_wrap_html_content(self):
        data = {}
        data["html"] = {}
        data["html"]["header"] = "header"
        data["html"]["aside"] = "aside"
        data["html"]["footer"] = "footer"
        expected_output = "headercontentasidefooter"
        self.assertEqual(tools.wrap_html_content("content", data["html"]), expected_output)

    def test_check_json(self):
        self.assertTrue(tools.check_json({"title":"", "text":"", "tags":"", "timestamp":""}))
    def test_check_json_falseness(self):
        self.assertFalse(tools.check_json({"title":"", "text":"", "timestamp":""}))

if __name__ == '__main__':
    unittest.main()